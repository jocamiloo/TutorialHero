import { Hero } from './hero';

export const HEROES: Hero[] = [
  { numeroDocumento: '11', nombres: 'Mr. Nice', apellidos: 'power', telefono: '123', celular: '980' },
  { numeroDocumento: '12', nombres: 'Narco', apellidos: 'Orange', telefono: '324', celular: '789' },
  { numeroDocumento: '13', nombres: 'Bombasto', apellidos: 'Bomber',telefono: '234', celular: '678' },
  { numeroDocumento: '14', nombres: 'Celeritas', apellidos: 'Vela', telefono: '3456', celular: '094'},
  { numeroDocumento: '15', nombres: 'Magneta', apellidos: 'Magnesio', telefono: '456678', celular: '3452' },
  { numeroDocumento: '16', nombres: 'RubberMan', apellidos: ' Ruby',telefono: '3423', celular: '2356' },
  { numeroDocumento: '17', nombres: 'Dynama', apellidos: 'Diamon',telefono: '2345', celular: '2346' },
  { numeroDocumento: '18', nombres: 'Dr IQ', apellidos: 'Doctor',telefono: '4355', celular: '3467' },
  { numeroDocumento: '19', nombres: 'Magma', apellidos: 'Magneton',telefono: '345', celular: '3457' },
  { numeroDocumento: '20', nombres: 'Tornado', apellidos: 'Strong',telefono: '234', celular: '4356' }
];